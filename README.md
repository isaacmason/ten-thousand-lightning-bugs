# Ten thousand lightning bugs

[View on GitLab Pages](https://isaacmason.gitlab.io/ten-thousand-lightning-bugs/)

This is a Three.js experiment that renders ten thousand "lightning bugs" or particles going on a random walk. The experiment also features an embedded SoundCloud playlist with some of the best renditions of the song Fireflies that are out there.
