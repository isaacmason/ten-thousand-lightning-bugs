// Ten Thousand Lightning Bugs
// Displays ten thousand lightning bugs and plays the song Fireflies

var camera, scene, renderer, light, firefliesGeometry, domElement;
var firefliesCount = 10000;
setup();

/**
 * Sets up the three.js scene components and starts rendering after setup
 */
function setup() {
    // Create the scene
    scene = new THREE.Scene();

    // Create the camera
    camera = createCamera();

    // Create the light
    light = createLight();
    scene.add( light );

    // Create the cube mesh
    fireflies = createFireflies();
    scene.add( fireflies );

    // Create the renderers
    renderer = createRenderer();

    // Create the event listener for on resize
    window.addEventListener( 'resize', onWindowResize, false );
    onWindowResize();

    // Start the render loop
    animate();
}

/**
 * Creates the camera for the scene
 */
function createCamera() {
    var camera = new THREE.PerspectiveCamera( 70, 1, 1, 1000 );
    camera.position.z = 500;
    return camera;
}

/**
 * Creates the lighting for the scene
 */
function createLight() {
    var lights = new THREE.Group();

    var directionalLight = new THREE.DirectionalLight( 0xffffff, 2 );
    directionalLight.position = new THREE.Vector3(  300, 0, 300 );
    directionalLight.lookAt( new THREE.Vector3( 0, 0, 0 ) );
    lights.add( directionalLight );

    var ambientLight = new THREE.AmbientLight( 0xffffff, 1 );
    ambientLight.position = new THREE.Vector3(  0, -200, 400 );
    ambientLight.lookAt( new THREE.Vector3( 0, 0, 0 ) );
    lights.add( ambientLight );

    return lights;
}

/**
 * Creates the fireflies PointCloud for the scene
 */
function createFireflies() {
    // Create material
    var firefliesMaterial = new THREE.PointsMaterial({
        color: 0xffff85,
        size: 1,
        map: THREE.ImageUtils.loadTexture(
            'particle.png'),
        transparent: true,
        sizeAttenuation: true,
        alphaTest: 0.5,
    });

    // Create geometry
    var max = 900;
    var centerOffset = 450;
    firefliesGeometry = new THREE.Geometry();
    for (var i = 0; i < firefliesCount; i++) {
        firefliesGeometry.vertices.push(new THREE.Vector3(
            (Math.random() * max) - centerOffset, // x
            (Math.random() * max) - centerOffset, // y
            (Math.random() * max) - centerOffset  // z
        ));
    };

    // Create a pointcloud for the fireflies
    var fireflies = new THREE.Points(firefliesGeometry, firefliesMaterial);
    
    return fireflies;
}

/**
 * Creates the renderer
 */
function createRenderer() {
    var rendererContainer = document.querySelector("#renderer");
    var renderer = new THREE.WebGLRenderer({ 
        antialias: true,        
    });
    renderer.setPixelRatio( window.devicePixelRatio );
    rendererContainer.prepend( renderer.domElement );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.shadowMapEnabled = true;

    return renderer;
}

/**
 * Handles window resize events
 */
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.getElementById('titles').style.height = document.querySelector('#renderer').clientHeight + "px";
}

/**
 * Runs each run of the render loop
 */
function animate() {
    requestAnimationFrame( animate );

    // Animate the fireflies
    var updateCount = 1000;
    var randomIndex;
    for (var i = 0; i < updateCount; i++) {
        randomIndex = Math.floor(Math.random() * firefliesCount);
        TweenMax.to(firefliesGeometry.vertices[randomIndex], .5, {
            x: firefliesGeometry.vertices[randomIndex].x + Math.random() * 3 - 1.5, // x
            y: firefliesGeometry.vertices[randomIndex].y + Math.random() * 3 - 1.5, // y
            z: firefliesGeometry.vertices[randomIndex].z + Math.random() * 3 - 1.5, // z
        });
    }
    firefliesGeometry.verticesNeedUpdate = true;

    // Update the camera position based on the page y offset
    var pageScrollRotationDegrees = window.pageYOffset / 600;
    TweenMax.to(camera.position, .5, {
        x: Math.cos( pageScrollRotationDegrees ) * 200,
        y: Math.cos( pageScrollRotationDegrees ) * 200,
        onUpdate: () => {
            camera.lookAt( scene.position );
        }
    });

    // Render the scene with the camera
    renderer.render( scene, camera );
}

/*
 * Scrolls the SoundCloud page section into view smoothly
 */
function smoothScrollToSoundcloud() {
    document.querySelector('#soundcloud').scrollIntoView({
        behavior: 'smooth'
    });
}
